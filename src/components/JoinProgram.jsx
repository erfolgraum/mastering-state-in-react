import React, { useState } from "react";

  const JoinProgram = ({ email }) => {
    const [subscribed, setSubscribed] = useState(false);
    const [unsubscribed, setUnsubscribed] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const [loading, setLoading] = useState(false);

    const handleOnSubmit = (e) => {
      e.preventDefault();
      setSubscribed(true);
      setInputValue("");
    };

    const handleSubscribe = () => {
      if (!loading) {
        setLoading(true);
        const url = "/subscribe";
        const data = { email };
        const options = {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        };
        fetch(url, options)
          .then((response) => response.json())
          .then((result) => {
            if (result.success) {
              setSubscribed(true); // Update the subscribed state variable
              console.log(data, "Res:", result);
            }
          })
          .catch((error) => {
            console.error(error);
          });
      }
      // Send a POST request to the /subscribe endpoint
    };

    const handleUnsubscribe = () => {
      if (!loading) {
        setLoading(true);
        // Send a POST request to the /unsubscribe endpoint
        const url = "/unsubscribe";
        const data = { email };
        const options = {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        };
        fetch(url, options)
          .then((response) => response.json())
          .then((result) => {
            if (result.success) {
              setUnsubscribed(true); // Update the subscribed state variable
            }
          })
          .catch((error) => {
            console.error(error);
          })
          .finally(() => {
            setLoading(false);
          });
      }
    };

    if (subscribed) {
      return (
        <section className="app-section--join-program">
          <h2 className="app-title app-title--join-section">Join Our Program</h2>
          <h3 className="app-subtitle app-subtitle--join-section">
            Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </h3>
          <form className="form-join" onSubmit={handleOnSubmit}>
            <input
              className="email"
              placeholder="Email"
              type="email"
              name="emailaddress"
              required="required"
              value={email}
              onChange={(e) => {
                setInputValue(e.target.value);
              }}
            />
            {/* <button className="app-section__button app-section__button--subscribe" onClick={subscribeUser} disabled={isPending} style={{ opacity: isPending ? 0.5 : 1 }}>Subscribe</button>
           <button className="app-section__button app-section__button--subscribe" onClick={unsubscribeUser} disabled={isPending} style={{ opacity: isPending ? 0.5 : 1 }}>Unsubscribe</button> */}
            <button
              className="app-section__button app-section__button--subscribe"
              onClick={handleUnsubscribe}
            >
              Unsubscribe
            </button>
          </form>
        </section>
      );
    }

    if (unsubscribed) {
      return (
        <section className="app-section--join-program">
          <h2 className="app-title app-title--join-section">Join Our Program</h2>
          <h3 className="app-subtitle app-subtitle--join-section">
            Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </h3>
          <form className="form-join" onSubmit={handleOnSubmit}>
            <input
              className="email"
              placeholder="Email"
              type="email"
              name="emailaddress"
              required="required"
              value={email}
              onChange={(e) => {
                setInputValue(e.target.value);
              }}
            />
            {/* <button className="app-section__button app-section__button--subscribe" onClick={subscribeUser} disabled={isPending} style={{ opacity: isPending ? 0.5 : 1 }}>Subscribe</button>
           <button className="app-section__button app-section__button--subscribe" onClick={unsubscribeUser} disabled={isPending} style={{ opacity: isPending ? 0.5 : 1 }}>Unsubscribe</button> */}
            <button
              className="app-section__button app-section__button--subscribe"
              onClick={handleSubscribe}
            >
              Unsubscribe
            </button>
          </form>
        </section>
      );
    }

    return (
      <section className="app-section--join-program">
       <h2 className="app-title app-title--join-section">Join Our Program</h2>
       <h3 className="app-subtitle app-subtitle--join-section">
         Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
       </h3>
       <form className="form-join" onSubmit={handleOnSubmit}>
         <input
           className="email"
           placeholder="Email"
           type="email"
           name="emailaddress"
           required="required"
           value={inputValue}
           onChange={(e) => {
             setInputValue(e.target.value);
           }}
         />
         {/* <button className="app-section__button app-section__button--subscribe" onClick={subscribeUser} disabled={isPending} style={{ opacity: isPending ? 0.5 : 1 }}>Subscribe</button>
         <button className="app-section__button app-section__button--subscribe" onClick={unsubscribeUser} disabled={isPending} style={{ opacity: isPending ? 0.5 : 1 }}>Unsubscribe</button> */}
        <button className="app-section__button app-section__button--subscribe" onClick={handleSubscribe}>Subscribe</button>
       </form>
     </section>
    )
  };



export default JoinProgram;
